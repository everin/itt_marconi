CREATE TABLE `PhotoAlbum` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificativo univoco',
  `username` varchar(50) NOT NULL COMMENT 'Utente che ha inserito la foto',
  `description` varchar(1000) NOT NULL COMMENT 'Descrizione della foto',
  `photo` mediumblob COMMENT 'Foto BLOB',
  PRIMARY KEY (`id`),
  KEY `PhotoAlbum_username_IDX` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;